import React, { Component } from "react";
import axios from "axios";
import "./address.css";
import { fetchAddressList, fetchRegionList, AddAddress } from "./api";
import { GoogleApiWrapper } from 'google-maps-react' 
import MapContainer from './MapContainer'

class Address extends Component {
  constructor(props) {
    super(props);
    this.state = {
      regions: [],
      address: "",
      phone: "",
      mobile_no: "",
      lat: 0,
      lng: 0,
      coordinator_company_name: "",
      coordinator_person_name: "",
      region_id: ""
    };
  }
  componentWillMount() {
    let me = this;
    fetchRegionList().then(
      data => {
        me.setState({ regions: data.region_list });
      },
      () => {}
    );
    
  }
  onSubmit(e) {
    e.preventDefault();

    AddAddress({
      address: this.state.address,
      phone: this.state.phone,
      mobile_no: this.state.mobile_no,
      lat: 1,
      lng: 1,
      coordinator_company_name: this.state.coordinator_company_name,
      coordinator_person_name: this.state.coordinator_person_name,
      region_id: "1"
    }).then(
      data => {
        console.log(data);
        this.setState({
          address: "",
          phone: "",
          mobile_no: "",
          lat: 0,
          lng: 0,
          coordinator_company_name: "",
          coordinator_person_name: "",
          region_id: ""
        });
      },
      e => {
        let messages = e.response.data.error_messages
          .map(function(elem) {
            return elem.error_msg;
          })
          .join(",");

        alert(messages);
      }
    );
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value }, () => {});
  }

  render() {
    return (
      <div className="container">
        <div className="address-container">
          <form onSubmit={this.onSubmit.bind(this)} className="col-md-6">
            <div className="form-group">
              <label>Name</label>
              <input
                value={this.state.coordinator_person_name}
                type="text"
                className="form-control"
                id="coordinator_person_name"
                name="coordinator_person_name"
                placeholder="Name"
                onChange={this.handleChange.bind(this)}
              />
            </div>
            <div className="form-group">
              <label>Company</label>
              <input
                value={this.state.coordinator_company_name}
                type="text"
                className="form-control"
                id="coordinator_company_name"
                name="coordinator_company_name"
                placeholder="Company"
                onChange={this.handleChange.bind(this)}
              />
            </div>

            <div className="form-group">
              <label>Address</label>
              <input
                value={this.state.address}
                type="text"
                className="form-control"
                id="address"
                name="address"
                placeholder="address"
                onChange={this.handleChange.bind(this)}
              />
            </div>
            <div className="form-group">
              <label>Phone</label>
              <input
                value={this.state.phone}
                type="text"
                className="form-control"
                id="phone"
                name="phone"
                placeholder="phone"
                onChange={this.handleChange.bind(this)}
              />
            </div>
            <div className="form-group">
              <label>Mobile</label>
              <input
                value={this.state.mobile_no}
                type="text"
                className="form-control"
                id="mobile_no"
                name="mobile_no"
                placeholder="mobile"
                onChange={this.handleChange.bind(this)}
              />
            </div>

            <div className="form-group">
              <label>Regions</label>
              <select
                className="form-control"
                name="region_id"
                value={this.state.region_id}
                onChange={this.handleChange.bind(this)}
              >
                <option />
                {this.state.regions.map(item => {
                  return (
                    <option value={item.id} key={item.id}>
                      {item.region_name}
                    </option>
                  );
                })}
              </select>
            </div>

            <input type="submit" className="btn save-button" value="Save" />
          </form>
        </div>
        <div className="row">
          <div className="col-md-12">
            <div style={{ height: "500px", width: "300px" }} >

        <MapContainer google={this.props.google} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}


export default GoogleApiWrapper({
  apiKey: 'AIzaSyBPEPY_YhYAG9MXlpQkN4KtKQkRzfWCs0A',
})(Address)

