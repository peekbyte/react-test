import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import Address from "./address";
import { fetchAddressList, fetchRegionList, AddAddress } from "./api";
import { compose, withProps } from "recompose"
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addresses: []
    };
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value }, () => {});
  }

  componentWillMount() {
    let me = this;
    fetchAddressList().then(
      data => {
        console.log("address_list", data.address_list);
        me.setState({addresses: data.address_list.slice(1, 6)})
      },
      () => {}
    );
  }
  render() {
    return (
        <div className="container">
          <div className="row">
            <a className="btn  btn-xs login-button" href="/address" role="button">Add Address</a>
          </div>
          <div className="address-container">
            {this.state.addresses.map((item, i) => {
              return (
                <div className='address-row'>
                  {" "}
                  <div>{item.address} <input name="selected" value={item.selected} onChange={this.handleChange.bind(this)}  type="checkbox"/></div>
                  <div></div>
                  <table className="table">
                    <thead>
                      <tr>
                        <th>row </th>
                        <th>Company</th>
                        <th>Phone </th>
                        <th>Mobile </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr key={item.id}>
                        <td> {i + 1}</td>
                        <td>{item.compony}</td>
                        <td>{item.phone}</td>
                        <td>{item.mobile}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              );
            })}
          </div>
          <input type="button" className="btn save-button" value="Continue" />
      </div>
    );
  }
}

export default App;
