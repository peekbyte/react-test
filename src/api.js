import axios from 'axios';

export function fetchAddressList() {
    const config = { headers: { 
    'Content-Type': 'application/json',
    'APIToken': 'AC2zToa6934',
    'USERTOKEN': '2b5a659e5d37a95a7f38ac3e1ae038'
 } }
    return new Promise((resolve, reject) => {
        axios.post('http://52.27.230.100/api/caravan/address_list',{},config).then((response) => {
            resolve(response.data);
        }).catch((error) => {
            reject(error);
        })
    })
}

export function fetchRegionList() {
    const config = { headers: { 
    'Content-Type': 'application/json',
    'APIToken': 'AC2zToa6934',
    'USERTOKEN': '2b5a659e5d37a95a7f38ac3e1ae038'
 } }
    return new Promise((resolve, reject) => {
        axios.post('http://52.27.230.100/api/caravan/region_list',{}, config).then((response) => {
            resolve(response.data);
        }).catch((error) => {
            reject(error);
        })
    })
}

export function AddAddress(data) {
    const config = { headers: { 
    'Content-Type': 'application/json',
    'APIToken': 'AC2zToa6934',
    'USERTOKEN': '2b5a659e5d37a95a7f38ac3e1ae038'
 } }
    return new Promise((resolve, reject) => {
        axios.post('http://52.27.230.100/api/caravan/add_address',data, config).then((response) => {
            resolve(response.data);
        }).catch((error) => {
            reject(error);
        })
    })
}